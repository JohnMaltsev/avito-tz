package testlog

type MergeLogs struct {
	TestName          string `json:"test_name"`
	TestStatus        string `json:"test_status"`
	ExpectedTestValue string `json:"expected"`
	ActualTestValue   string `json:"actual"`
}
