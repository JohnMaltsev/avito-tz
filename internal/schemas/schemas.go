package schemas

type Logs struct {
	Time   string `json:"time"`
	Test   string `json:"test"`
	Output string `json:"output"`
}
