package main

import (
	"log"
)

var checkTests, logs1, logs2 map[string][]interface{}
var err error

func init() {

	if logs1, err = ReadFiles("../../config/testlog1.json"); err != nil {
		log.Fatalln("Error reading config: ", err)
	}

	if logs2, err = ReadFiles("../../config/testlog2.json"); err != nil {
		log.Fatalln("Error reading config: ", err)
	}

	if checkTests, err = ReadFiles("../../config/check-tests.json"); err != nil {
		log.Fatalln("Error reading config: ", err)
	}

	for _, v := range logs1 {
		log.Println("-----------", v)

	}

	// keys := make([]string, 0, len(logs1))
	// values := make([]interface{}, 0, len(logs1))

	// for k, v := range logs1 {
	// 	keys = append(keys, k)
	// 	values = append(values, v)

	// 	log.Println("keys ---", keys)
	// 	log.Println("values ---", values)
	// }

	// for _, v := range values {
	// 	// if v == "time" {
	// 	// 	log.Println("!!!! time", v)
	// 	// }

	// 	if x, ok := v["time"]; ok {

	// 	}
	// 	// log.Println("!!!! time", v["time"])

	// }

}

func main() {
	log.Println("logs1 ", logs1)
	log.Println("logs2 ", logs2)
	log.Println("checkTests ", checkTests)
}
