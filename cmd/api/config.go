package main

import (
	"encoding/json"

	// "github.com/niklucky/go-lib"
	"io/ioutil"
)

type Config struct {
	Logs    string `json:"logs"`
	Suites  string `json:"suites"`
	Catures string `json:"captures"`
}

func ReadFiles(fileName string) (map[string][]interface{}, error) {
	var data map[string][]interface{}

	fileData, err := ioutil.ReadFile(fileName)
	if err != nil {
		return data, err
	}

	err = json.Unmarshal(fileData, &data)
	if err != nil {
		return data, err
	}
	return data, nil
}
